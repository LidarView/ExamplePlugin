#ifndef vtkExamplePacketInterpreter_h
#define vtkExamplePacketInterpreter_h

#include <vtkUnsignedCharArray.h>
#include <vtkUnsignedIntArray.h>
#include <vtkUnsignedShortArray.h>
#include <vtkUnsignedLongArray.h>
#include <vtkStringArray.h>
#include <vtkTypeInt64Array.h>

#include <map>

#include "vtkLidarPacketInterpreter.h"

struct LaserCorrection
{
  double verticalAngle;
  double horizontalAngle;
};

using laser_idx_t = std::pair<unsigned int, unsigned int>;

class VTK_EXPORT vtkExamplePacketInterpreter : public vtkLidarPacketInterpreter
{
public:
  static vtkExamplePacketInterpreter* New();
  vtkTypeMacro(vtkExamplePacketInterpreter, vtkLidarPacketInterpreter);

  void LoadCalibration(const std::string& filename) override;

  void ProcessPacket(unsigned char const * data, unsigned int dataLength) override;

  bool IsLidarPacket(unsigned char const * data, unsigned int dataLength) override;

  bool PreProcessPacket(unsigned char const * data, unsigned int dataLength,
                        fpos_t filePosition = fpos_t(), double packetNetworkTime = 0,
                        std::vector<FrameInformation>* frameCatalog = nullptr) override;

  std::string GetSensorInformation(bool shortVersion = false) override;

protected:
  vtkSmartPointer<vtkPolyData> CreateNewEmptyFrame(vtkIdType numberOfPoints, vtkIdType prereservedNumberOfPoints = 60000) override;

  vtkSmartPointer<vtkPoints> Points;
  vtkSmartPointer<vtkDoubleArray> PointsX;
  vtkSmartPointer<vtkDoubleArray> PointsY;
  vtkSmartPointer<vtkDoubleArray> PointsZ;
  vtkSmartPointer<vtkUnsignedIntArray> Intensity;
  vtkSmartPointer<vtkUnsignedIntArray> ChannelID;
  vtkSmartPointer<vtkUnsignedIntArray> Azimuth;
  vtkSmartPointer<vtkDoubleArray> Distance;
  vtkSmartPointer<vtkTypeInt64Array> Timestamp;
  vtkSmartPointer<vtkDoubleArray> VerticalAngle;


  vtkExamplePacketInterpreter();
  ~vtkExamplePacketInterpreter();

private:  
  //! @brief Calibration of each laser
   std::map<laser_idx_t, LaserCorrection> Calibration;

  vtkExamplePacketInterpreter(const vtkExamplePacketInterpreter&) = delete;
  void operator=(const vtkExamplePacketInterpreter&) = delete;

};

struct ExampleSpecificFrameInformation : public SpecificFrameInformation
{
  // Keep the frame id
  u_int32_t frameID = 0;

  void reset() override { *this = ExampleSpecificFrameInformation(); }
  std::unique_ptr<SpecificFrameInformation> clone() override { return std::make_unique<ExampleSpecificFrameInformation>(*this); }
};

#endif // vtkExamplePacketInterpreter_h
