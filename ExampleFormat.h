#ifndef ExampleFormat_h
#define ExampleFormat_h

#include <boost/predef/other/endian.h> 
#include <boost/endian/arithmetic.hpp>
#include <memory>

#include "InterpreterHelper.h"

// This file contains the structure of the packet format.
// You need to use "pragma pack (push, 1)" and "pragma pack (pop)"
// so there is no added padding between variables.
// The function BF_GET (define in InterpreterHelper.h) allows to divide a byte
// to read a variable smaller than 8 bits (see Flag1 & Flag 2 bellow).

// The example sensor is a rotational sensor with 64 lasers and 180 values of azimuth
// (one firings every 2 degrees)
static constexpr unsigned int numberOfChannels{64};
static constexpr unsigned int numberOfAzimuths{180};

#pragma pack(push, 1)
//! @brief class representing the Example packet header
/*
   0               1               2               3
   0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |                            FrameID                            |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  | Flag1  | Flag2 |     Info      |          Reserved            |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
class PacketHeader
{
private:
  // Frame ID : Number of the current frame
  boost::endian::big_uint32_t FrameID;

  // 4 bits Flag1
  // 4 bits Flag2
  boost::endian::big_uint8_t Flag1_Flag2;

  // InfoC
  boost::endian::big_uint8_t Info;

  // Reserved
  boost::endian::big_uint16_t Reserved;

public:
  GET_NATIVE_UINT(32, FrameID)
  SET_NATIVE_UINT(32, FrameID)
  GET_NATIVE_UINT(8, Info)
  SET_NATIVE_UINT(8, Info)
  GET_NATIVE_UINT(16, Reserved)
  SET_NATIVE_UINT(16, Reserved)

  uint8_t GetFlag2() const
  {
    return BF_GET(Flag1_Flag2, 0, 4);
  }

  uint8_t GetFlag1() const
  {
    return BF_GET(Flag1_Flag2, 4, 4);
  }

};
#pragma pack(pop)


#pragma pack(push, 1)
//! @brief class representing the Example firing return
/*
   0               1               2               3
   0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |    ChannelID   | Reflectivity  |            Distance           |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |    Intensity   |
  +-+-+-+-+-+-+-+-+-
 */
class FiringBlock
{
private:
  // Channel ID
  boost::endian::big_uint8_t ChannelID;

  // Reflectivity
  boost::endian::big_uint8_t Reflectivity;

  // Distance of the return
  boost::endian::big_uint16_t Distance;

  // Intensity
  boost::endian::big_uint8_t Intensity;

public:
  GET_NATIVE_UINT(8, ChannelID)
  SET_NATIVE_UINT(8, ChannelID)
  GET_NATIVE_UINT(8, Reflectivity)
  SET_NATIVE_UINT(8, Reflectivity)
  GET_NATIVE_UINT(16, Distance)
  SET_NATIVE_UINT(16, Distance)
  GET_NATIVE_UINT(8, Intensity)
  SET_NATIVE_UINT(8, Intensity)
};
#pragma pack(pop)

#pragma pack(push, 1)
//! @brief class representing the Example firing return
/*
   0               1               2               3
   0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |                           Timestamp                           |
  |                                                               |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |            Azimuth             |          Reserved            |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
class AllFiringsBlock
{
private:

  // Timestamp : Timestamps in seconds
  boost::endian::big_uint64_t Timestamp;

  // Azimuth of the firing block
  boost::endian::big_uint16_t Azimuth;

  // Reserved
  boost::endian::big_uint16_t Reserved;

  // Every firings
  FiringBlock firings[numberOfChannels];

public:
  GET_NATIVE_UINT(64, Timestamp)
  SET_NATIVE_UINT(64, Timestamp)
  GET_NATIVE_UINT(16, Azimuth)
  SET_NATIVE_UINT(16, Azimuth)
  GET_NATIVE_UINT(16, Reserved)
  SET_NATIVE_UINT(16, Reserved)

  FiringBlock GetFiring(unsigned int i) const
  {
    return firings[i];
  }
};
#pragma pack(pop)


#pragma pack(push, 1)
//! @brief class representing the Example Packet
struct LidarPacket{
  PacketHeader header;
  AllFiringsBlock firing[numberOfAzimuths];

};
#pragma pack(pop)




#endif // ExampleFormat_h
