#include "vtkExamplePacketInterpreter.h"

#include "ExampleFormat.h"

#include <vtkDelimitedTextReader.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkTransform.h>

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkExamplePacketInterpreter)

//-----------------------------------------------------------------------------
vtkExamplePacketInterpreter::vtkExamplePacketInterpreter()
{
  this->ParserMetaData.SpecificInformation = std::make_shared<ExampleSpecificFrameInformation>();

  this->ResetCurrentFrame();
}

//-----------------------------------------------------------------------------
vtkExamplePacketInterpreter::~vtkExamplePacketInterpreter()
{

}

//-----------------------------------------------------------------------------
// The function LoadCalibration should read and save information from the calibration file
// Those saved information should be used in the computation of the point position
// in ProcessPacket function
//-----------------------------------------------------------------------------
void vtkExamplePacketInterpreter::LoadCalibration(std::string const & filepath)
{
  if (filepath.empty())
  {
    this->IsCalibrated = false;
    return;
  }

  // Load the CSV file.
  vtkNew<vtkDelimitedTextReader> reader;
  reader->SetFileName(filepath.c_str());
  reader->DetectNumericColumnsOn();
  reader->SetHaveHeaders(false);
  reader->SetFieldDelimiterCharacters(",");
  reader->SetStringDelimiter('"');
  reader->Update();

  // Extract the table.
  vtkTable * csvTable = reader->GetOutput();
  vtkIdType nRows = csvTable->GetNumberOfRows();

  this->CalibrationData->ShallowCopy(csvTable);

  for (vtkIdType indexRow = 0; indexRow < nRows; ++indexRow)
  {
    LaserCorrection correction;
    correction.horizontalAngle = this->CalibrationData->GetValue(indexRow, 2).ToInt();
    correction.verticalAngle = this->CalibrationData->GetValue(indexRow, 3).ToInt();

    laser_idx_t idx = std::make_pair(
      static_cast<unsigned int>(this->CalibrationData->GetValue(indexRow, 1).ToInt()),
      static_cast<unsigned int>(this->CalibrationData->GetValue(indexRow, 0).ToInt()));

    this->Calibration[idx] = correction;
  }
  this->IsCalibrated = true;
}

//-----------------------------------------------------------------------------
// The function ProcessPacket
// Add the information (points and raw information) of the network packet to the current frame
// This function is not called in reader mode and in stream mode
// You need to detect and handle new frame in this function
// You also need to add all packet information that you want to expose in the spreadsheet
//-----------------------------------------------------------------------------
void vtkExamplePacketInterpreter::ProcessPacket(unsigned char const* data,
                                                unsigned int dataLength)
{
  if (!this->IsLidarPacket(data, dataLength))
  {
    return;
  }

  ExampleSpecificFrameInformation* frameInfo = reinterpret_cast<ExampleSpecificFrameInformation*>(this->ParserMetaData.SpecificInformation.get());

  const LidarPacket* dataPacket = reinterpret_cast<const LidarPacket*>(data);


  // Detection of a new frame
  // In this example a frame is detected thanks to a field "frame id" in the header of the packet.
  // This means that in this example there is no new frame detected inside a unique packet.
  // In some lidar you need to detect a rollback of the azimuth to detect a new frame
  // In the interpreter you have an option to apply a transform, display zero distance and crop data
  // This functionnalities should be handle here
  if (dataPacket->header.GetFrameID() != frameInfo->frameID)
  {
    this->SplitFrame();
  }

  // Update the transforms here and then call internal transform
  if (SensorTransform) this->SensorTransform->Update();

  for (unsigned int curr_azimuth = 0; curr_azimuth < numberOfAzimuths; curr_azimuth++)
  {
    unsigned int azimuth = dataPacket->firing[curr_azimuth].GetAzimuth();
    uint64_t timestamp = dataPacket->firing[curr_azimuth].GetTimestamp();

    for (unsigned int curr_channel = 0; curr_channel < numberOfChannels; curr_channel++)
    {
      laser_idx_t idx = std::make_pair(curr_azimuth, curr_channel);
      float horizontalAngle = this->Calibration[idx].horizontalAngle;
      float verticalAngle = this->Calibration[idx].verticalAngle;

      float distance = dataPacket->firing[curr_azimuth].GetFiring(curr_channel).GetDistance() ;

      float x = distance * std::sin(horizontalAngle) * std::cos(verticalAngle);
      float y = distance * std::sin(verticalAngle);
      float z = distance * std::cos(horizontalAngle) * std::cos(verticalAngle);

      double pos[3] = {x, y, z};

      // Apply sensor transform
      if (SensorTransform) this->SensorTransform->InternalTransformPoint(pos, pos);

      // Test if the point should be displayed
      if ((this->IgnoreZeroDistances && distance == 0) || this->shouldBeCroppedOut(pos))
      {
        return;
      }

      this->Points->InsertNextPoint(pos);
      InsertNextValueIfNotNull(this->PointsX, x);
      InsertNextValueIfNotNull(this->PointsY, y);
      InsertNextValueIfNotNull(this->PointsZ, z);
      InsertNextValueIfNotNull(this->Azimuth, azimuth);
      InsertNextValueIfNotNull(this->Intensity, dataPacket->firing[curr_azimuth].GetFiring(curr_channel).GetIntensity());
      InsertNextValueIfNotNull(this->ChannelID, dataPacket->firing[curr_azimuth].GetFiring(curr_channel).GetChannelID());
      InsertNextValueIfNotNull(this->Timestamp, timestamp);
      InsertNextValueIfNotNull(this->Distance, distance);
    }
  }
}

//-----------------------------------------------------------------------------
// The function IsLidarPacket return true if the interpreter can read the current packet
// You can first test that the length of the received packet is valid
// In addition to that check, you could verify some flags of the packet
//-----------------------------------------------------------------------------
bool vtkExamplePacketInterpreter::IsLidarPacket(unsigned char const * data,
                                                unsigned int dataLength)
{
  bool validSize = dataLength == sizeof(FiringBlock);

  const LidarPacket* dataPacket = reinterpret_cast<const LidarPacket*>(data);
  bool validFlag1 = dataPacket->header.GetFlag1() == 1;

  return validSize && validFlag1;
}

//-----------------------------------------------------------------------------
// The function CreateNewEmptyFrame reset a frame with its arrays
// This function is called automatically by the vtkLidarPacketInterpreter::SplitFrame
// If you rewrite the SplitFrame function, don't forget to call CreateNewEmptyFrame
//-----------------------------------------------------------------------------
vtkSmartPointer<vtkPolyData> vtkExamplePacketInterpreter::CreateNewEmptyFrame(vtkIdType numberOfPoints, vtkIdType prereservedNumberOfPoints)
{
  const int defaultPrereservedNumberOfPointsPerFrame = 60000;
  // prereserve for 50% points more than actually received in previous frame
  prereservedNumberOfPoints = std::max(static_cast<int>(prereservedNumberOfPoints * 1.5), defaultPrereservedNumberOfPointsPerFrame);

  vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();

  // Initialize points
  vtkNew<vtkPoints> points;
  points->SetDataTypeToFloat();
  points->Allocate(prereservedNumberOfPoints);
  if (numberOfPoints > 0 )
  {
    points->SetNumberOfPoints(numberOfPoints);
  }
  points->GetData()->SetName("Points");
  polyData->SetPoints(points.GetPointer());

  // Initialize data arrays
  this->Points = points.GetPointer();
  InitArrayForPolyData(true, this->PointsX, "X", numberOfPoints,
                       prereservedNumberOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(true, this->PointsY, "Y", numberOfPoints,
                       prereservedNumberOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(true, this->PointsZ, "Z", numberOfPoints,
                       prereservedNumberOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(false, this->Intensity, "intensity", numberOfPoints,
                       prereservedNumberOfPoints, polyData);
  InitArrayForPolyData(false, this->ChannelID, "channel", numberOfPoints,
                       prereservedNumberOfPoints, polyData);
  InitArrayForPolyData(false, this->Azimuth, "azimuth", numberOfPoints,
                       prereservedNumberOfPoints, polyData);
  InitArrayForPolyData(false, this->Distance, "distance", numberOfPoints,
                       prereservedNumberOfPoints, polyData);
  InitArrayForPolyData(false, this->Timestamp, "Timestamp", numberOfPoints,
                       prereservedNumberOfPoints, polyData);
  InitArrayForPolyData(false, this->VerticalAngle, "vertical_angle", numberOfPoints,
                       prereservedNumberOfPoints, polyData);

  // Set the default array to display in the application
  polyData->GetPointData()->SetActiveScalars("intensity");


  return polyData;
}

//-----------------------------------------------------------------------------
// The function PreProcessPacket fill the frame catalog in reader mode
// This function is not called in stream mode
// The frame catalogue contains all information to jump to one specific frame :
// - The file position where the frame begins
// - The network time of the first packet frame (use to display frame's time in the ui toolbar)
// - The first Packet time (use in the option vtkLidarReader::UsePacketTimeForDisplayTime
//-----------------------------------------------------------------------------
bool vtkExamplePacketInterpreter::PreProcessPacket(unsigned char const* data,
  unsigned int dataLength, fpos_t filePosition, double packetNetworkTime,
  std::vector<FrameInformation>* frameCatalog)
{
  const LidarPacket* dataPacket = reinterpret_cast<const LidarPacket*>(data);

  ExampleSpecificFrameInformation* frameInfo = reinterpret_cast<ExampleSpecificFrameInformation*>(this->ParserMetaData.SpecificInformation.get());

  // Detection of a new frame
  // In this example a frame is detected thanks to a field "frame id" in the header of the packet.
  // This means that in this example there is no new frame detected inside a unique packet.
  // In some lidar you need to detect a rollback of the azimuth to detect a new frame
  bool isNewFrame = dataPacket->header.GetFrameID() != frameInfo->frameID;
  if (isNewFrame)
  {
    // A new frame is detected, we flush the previous one
    this->SplitFrame();

    // We also update the information for the new frame
    // You need to set the file position to determine the beginning of a frame
    this->ParserMetaData.FilePosition = filePosition;
    this->ParserMetaData.FirstPacketDataTime = dataPacket->firing[0].GetTimestamp();
    this->ParserMetaData.FirstPacketNetworkTime = packetNetworkTime;

    // Push the nex frame into the frame catalog
    if (frameCatalog)
    {
      frameCatalog->push_back(this->ParserMetaData);
    }
  }

  return isNewFrame;
}

//-----------------------------------------------------------------------------
std::string vtkExamplePacketInterpreter::GetSensorInformation(bool vtkNotUsed(shortVersion))
{
  std::stringstream streamInfo;
  streamInfo << "Example Sensor " << numberOfChannels << " channels";

  return std::string(streamInfo.str());
}
